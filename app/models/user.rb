class User < ApplicationRecord

  has_many :microposts, dependent: :destroy

  def feed
    Micropost.where("user_id = ?", id)
  end

    private

end
